from audio_analysis import waveform
import os

# Test the creation of a waveform with arguments
def test_class_creation():
    inputFile = __file__[:-2] + "mp3"
    w = waveform.Waveform(
        audioFile=inputFile, width=1200, height=250, outPath="image/path"
    )
    assert w.audioFile == inputFile
    assert w.width == 1200
    assert w.height == 250
    assert w.outputPath == "image/path"


# Test the resize function for the image file
def test_resize():
    w = waveform.Waveform()
    w.resize(1000, 400)
    assert w.width == 1000
    assert w.height == 400


# Test the define input path function
def test_define_input():
    w = waveform.Waveform()
    inputFile = __file__[:-2] + "mp3"
    w.define_input_path(inputFile)
    assert w.audioFile == inputFile


# Test the define output path function
def test_define_output():
    w = waveform.Waveform()
    w.define_output_path("output/path")
    assert w.outputPath == "output/path"


def test_run():
    inputFile = __file__[:-2] + "mp3"
    outputFile = __file__[:-2] + "png"
    w = waveform.Waveform(
        audioFile=inputFile,
        width=1000,
        height=200,
        outPath=outputFile,
    )
    w.dump_image()
    assert os.path.exists(outputFile)
    os.remove(outputFile)
