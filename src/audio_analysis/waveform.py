"""
GoPro technical test.

The goal of this code is to generate as fast as possible waveform images after loading and preprocessing audio data.
A typical usecase would be live update of a resizable waveform in a UI.

Usage:
$ python technical_test.py my_music.mp3 200x50 waveform0.png 500x50 waveform1.png 1000x150 waveform2.png ...

"""
import os  # used to test if files exist
import numpy as np
import sys
import re
import subprocess
import json

import matplotlib.pyplot as plt  # used for debugging
from PIL import Image, ImageDraw

# Regular expression to handle image size arguments
DIMENSIONS_RE = re.compile(r"(?P<width>\d+)x(?P<height>\d+)")

# Bin used to analyze & read audio files
FFMPEG_BIN_PATH = "ffmpeg"
FFPROBE_BIN_PATH = "ffprobe"

# Margin between L/R waveforms in output image file
STEREO_PIXEL_MARGIN = 4

# Waveform class
class Waveform:
    # Initialization
    def __init__(
        self,
        audioFile: str = "",
        width: int = 500,
        height: int = 200,
        outPath: str = "waveform.png",
    ):
        if audioFile != "":
            assert os.path.exists(audioFile)
        self.audioFile = audioFile
        self.width = width
        self.height = height

        # Warning if the output path corresponds to a file that already exists
        if outPath != "" and os.path.exists(outPath):
            print("\n\t!!! Warning !!!")
            print(outPath + " already exists and will be overwritten !\n")
        self.outputPath = outPath

    # Display some info about current state of Waveform object
    def print(self):
        if self.audioFile != "":
            print("--> Input audio file \n\t" + self.audioFile)
        else:
            print("No audio file defined !")
        if self.outputPath != "":
            print("--> Output image file \n\t" + self.outputPath)
        else:
            print("No output image defined !")
        print("--> Waveform size \n\t", self.width, "x", self.height, "px")

    # Define the path to the input Audio file
    def define_input_path(self, audioFile: str):
        assert audioFile != ""
        assert os.path.exists(audioFile)
        self.audioFile = audioFile

    # Change the size of the waveform (in pixels)
    def resize(self, width: int, height: int):
        assert width > 0
        assert height > 0
        self.width = width
        self.height = height

    # Define the path to the output image file
    def define_output_path(self, outPath: str):
        assert outPath != ""
        if os.path.exists(outPath):
            print("\n\t!!! Warning !!!\n")
            print(outPath + " already exists and will be overwritten !")
        self.outputPath = outPath

    # Process audio and create the waveform image file
    def dump_image(self):
        # Print some infos before dumping image
        self.print()

        # Starting processing audio
        assert self.audioFile != ""
        assert os.path.exists(self.audioFile)
        print(f"Opening '{self.audioFile}'...")

        # Get audio file data
        nchannels, framerate, raw_audio_data = get_raw_audio_data(self.audioFile)
        audio_data = get_audio_data(nchannels, raw_audio_data)
        audio_duration = get_audio_duration(audio_data, framerate)
        print(
            f"nchannels = {nchannels}, framerate = {framerate}, audio duration = {audio_duration}"
        )
        # show_audio_data(audio_data)

        print(
            f"Generating waveform {self.width}x{self.height} in '{self.outputPath}'..."
        )
        # Compute audio level
        powers = get_audio_powers(audio_data, framerate, self.width)

        # Create image
        image = generate_waveform(powers, self.height)

        # Dump image
        image.save(self.outputPath)


# Get raw data from audio file through ffmpeg
def get_raw_audio_data(path):
    # probe informations
    probe_cmd = [
        FFPROBE_BIN_PATH,
        "-v",
        "error",
        "-of",
        "json",
        "-show_streams",
        path,
    ]
    data = subprocess.check_output(probe_cmd)
    data = json.loads(data)
    streams = data["streams"]
    audio_streams = [stream for stream in streams if stream["codec_type"] == "audio"]
    if len(audio_streams) != 1:
        raise Exception(
            "Unsupported number of audio streams in file: %d found" % len(audio_streams)
        )
    audio_stream = audio_streams[0]
    nchannels = int(audio_stream["channels"])
    framerate = int(audio_stream["sample_rate"])

    # generate raw audio data
    ffmpeg_cmd = [
        FFMPEG_BIN_PATH,
        "-v",
        "error",
        "-i",
        path,
        "-ac",
        str(nchannels),
        "-ar",
        str(framerate),
        "-f",
        "s16le",
        "-",
    ]
    raw_audio_data = subprocess.check_output(ffmpeg_cmd)

    return nchannels, framerate, raw_audio_data


# Transform raw data into values between 0 & 1
# with the following structure ((chan1, chan2, chan3, ...), (chan1, chan2, chan3, ...), ...)
def get_audio_data(nchannels, raw_audio_data):
    # convert to flat array of values
    bytes_per_value = 2
    assert len(raw_audio_data) % bytes_per_value == 0
    normalizer = 1.0 / float(2**15)
    # values = (abs(normalizer * np.frombuffer(raw_audio_data, dtype="h"))).tolist()
    values = abs(normalizer * np.frombuffer(raw_audio_data, dtype="h"))
    assert len(values) % nchannels == 0
    # The shape of audio_data is as follows :
    #        [[chan1],[chan2],...] where chanX is an audio channel
    # Each audio channel is made of a list of sample values [s1,s2,s3, ...]
    audio_data = [
        values[channel_index::nchannels] for channel_index in range(nchannels)
    ]
    return audio_data


# Get nb of channels and nb of samples
def get_shape(audio_data):
    nframes = len(audio_data[0])
    if nframes > 0:
        nchannels = len(audio_data)
        # assert all(len(frame) == nchannels for frame in audio_data)
    else:
        nchannels = None
    return nframes, nchannels


# Get duration of audio file
def get_audio_duration(audio_data, framerate):
    nframes, nchannels = get_shape(audio_data)
    return nframes / float(framerate)


# Use matplotlib to show the waveform before creating image file
def show_audio_data(audio_data):
    # for debugging
    nframes, nchannels = get_shape(audio_data)
    # times = [i / float(nframes) for i in range(nframes)]
    # On limite les divisions
    factor = 1.0 / float(nframes)
    times = [i * factor for i in range(nframes)]
    base_ax = None
    for channel_index in range(nchannels):
        ax = plt.subplot(
            nchannels, 1, channel_index + 1, sharex=base_ax, sharey=base_ax
        )
        if base_ax is None:
            base_ax = ax
        channel_data = [frame[channel_index] for frame in audio_data]
        plt.plot(times, channel_data)
    plt.show()


# Mix 2 values with a given weight
def mix(a, b, rate):
    return a + (b - a) * rate


# Crop value below x0 and above x1
def clamp(x, x0, x1):
    return max(x0, min(x1, x))


def get_audio_powers(
    integral_audio_data,
    framerate,
    nbins,
    bin_start=None,
    bin_end=None,
    start_time=None,
    end_time=None,
):
    nframes, nchannels = get_shape(integral_audio_data)
    audio_duration = get_audio_duration(integral_audio_data, framerate)

    start_time = 0.0 if start_time is None else start_time
    end_time = audio_duration if end_time is None else end_time
    bin_start = 0 if bin_start is None else bin_start
    bin_end = nbins if bin_end is None else bin_end

    powers = [
        [0 for frame in range(bin_end - bin_start)] for channel in range(nchannels)
    ]

    if bin_end == bin_start:
        return powers

    else:
        for bin_index in range(bin_start, bin_end):
            if bin_index < 0 or bin_index >= nbins:
                continue
            elif nbins == 1:
                slice_start = 0
                slice_end = nframes - 1
            else:
                half_width_in_samples = (
                    (end_time - start_time) * framerate / float(nbins - 1)
                )
                bin_ratio = bin_index / float(nbins - 1)
                time = mix(start_time, end_time, bin_ratio)
                center_index = time * framerate
                slice_start = int(round(center_index - half_width_in_samples))
                slice_end = int(round(center_index + half_width_in_samples))
                slice_start = clamp(slice_start, 0, nframes - 1)
                slice_end = clamp(slice_end, 0, nframes - 1)
                inv_slice_width = 1.0 / float(slice_end - slice_start)

            if slice_end > slice_start:
                tab = [
                    np.sum(integral_audio_data[channel_index][slice_start:slice_end])
                    for channel_index in range(nchannels)
                ]
                for channel_index in range(nchannels):
                    power = tab[channel_index]
                    powers[channel_index][bin_index - bin_start] = (
                        power * inv_slice_width
                    )
    return powers


# Draw the waveform(s) depending on channel count
def generate_waveform(
    powers, image_height, color=(255, 255, 255, 255), background_color=(0, 0, 0, 255)
):
    npowers, nchannels = get_shape(powers)
    image = Image.new(mode="RGBA", size=(npowers, image_height), color=background_color)
    draw = ImageDraw.Draw(image)

    for x in range(npowers):
        if nchannels == 1:
            half_height = image_height / 2

            power = powers[0][x]
            y_start = half_height - half_height * power
            y_end = half_height + half_height * power
            draw.line((x, y_start, x, y_end), fill=color)

        elif nchannels == 2:
            half_height = (image_height - STEREO_PIXEL_MARGIN) / 2

            power = powers[0][x]
            y_start = half_height
            y_end = y_start - half_height * power
            draw.line((x, y_start, x, y_end), fill=color)

            power = powers[1][x]
            y_start = image_height - half_height
            y_end = y_start + half_height * power
            draw.line((x, y_start, x, y_end), fill=color)
        else:
            raise NotImplementedError()

    return image


# Argument list analysis
def argument_analysis():
    if len(sys.argv) < 3:
        print("Arguments needed ! Syntax is as follows :")
        print("\t audio_file.[EXT] [WIDTH]x[HEIGHT] output_image.png")
        exit()

    input_path = sys.argv[1]
    output_argv = sys.argv[2:]
    assert len(output_argv) % 2 == 0
    outputs = []
    for size, output_path in zip(*(output_argv[i::2] for i in range(2))):
        match = DIMENSIONS_RE.match(size)
        assert match is not None, size
        width = int(match.group("width"))
        height = int(match.group("height"))
        outputs.append((width, height, output_path))

    return input_path, outputs


def main():
    # Parse command-line arguments
    audioFile, outputParam = argument_analysis()

    # Loop over expected configurations
    for param in outputParam:
        # Create a waveform object with given parameters
        w = Waveform(audioFile, param[0], param[1], param[2])
        # Process audio and dump image file
        w.dump_image()


if __name__ == "__main__":
    main()
