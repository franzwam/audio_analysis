# Audio Analysis Python Package
## Description
This library can analyze audio files through ffmpeg in order to create image files (.png) representing the waveforms.
Depending on the number of channels, the waveform will be built accordingly.

## List of dependencies :
- ffmpeg (and ffprobe)
- numpy
- matplotlib (for debugging purpose)
